﻿using System;
using System.ComponentModel.DataAnnotations;

namespace travel_agency.Models
{
    public class ViewModelPasajeros
    {
        [Key]
        public int id { get; set; }
        public int cedula { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public DateTime? fcreacion { get; set; }
        public DateTime? fmodificacion { get; set; }
    }
}
