﻿using System;
using System.ComponentModel.DataAnnotations;

namespace travel_agency.Models
{
    public class ViewModelViajes
    {
        [Key]
        public int id { get; set; }
        public int codigo { get; set; }
        public string origen { get; set; }
        public string destino { get; set; }
        public int disponibilidad { get; set; }
        public double precio { get; set; }
        public DateTime? fcreacion { get; set; }
        public DateTime? fmodificacion { get; set; }
    }
}
