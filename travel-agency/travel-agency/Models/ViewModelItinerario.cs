﻿using System;
using System.ComponentModel.DataAnnotations;

namespace travel_agency.Models
{
    public class ViewModelItinerario
    {
        [Key]
        public int id { get; set; }
        public int idPasajeros { get; set; }
        public int idVuelos { get; set; }
        public int cantidad { get; set; }
        public DateTime? fcreacion { get; set; }
        public DateTime? fmodificacion { get; set; }

        //public virtual ViewModelPasajeros pasajeroInfo { get; set; }
        //public virtual ViewModelViajes vieajesInfo { get; set; }
    }
}
