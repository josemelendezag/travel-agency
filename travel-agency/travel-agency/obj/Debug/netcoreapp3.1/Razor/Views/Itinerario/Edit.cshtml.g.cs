#pragma checksum "C:\Users\José Meléndez\Desktop\test\travel-agency\travel-agency\Views\Itinerario\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cb01280d95a4ae792f0bffb65347e068820264a8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Itinerario_Edit), @"mvc.1.0.view", @"/Views/Itinerario/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\José Meléndez\Desktop\test\travel-agency\travel-agency\Views\_ViewImports.cshtml"
using travel_agency;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\José Meléndez\Desktop\test\travel-agency\travel-agency\Views\_ViewImports.cshtml"
using travel_agency.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cb01280d95a4ae792f0bffb65347e068820264a8", @"/Views/Itinerario/Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9b8f6927c3f3e7fa5284bc8e1f455f9869e7b06d", @"/Views/_ViewImports.cshtml")]
    public class Views_Itinerario_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<travel_agency.Models.ViewModelItinerario>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\José Meléndez\Desktop\test\travel-agency\travel-agency\Views\Itinerario\Edit.cshtml"
 using (Html.BeginForm(ViewBag.Status != 0 ? "Modified" : "Insert", "Itinerario"))
{

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\José Meléndez\Desktop\test\travel-agency\travel-agency\Views\Itinerario\Edit.cshtml"
Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"docs-example-infocif\">\r\n    <div class=\"card border-info\">\r\n        <div class=\"card-header\">\r\n            <i class=\"fa fa-th-list\"> </i> Información sobre los pasajeros\r\n        </div>\r\n        <div class=\"card-body\">\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cb01280d95a4ae792f0bffb65347e068820264a84033", async() => {
                WriteLiteral(@"
                <div class=""form-group"">
                    <div class=""input-group mb-3"">
                        <div class=""input-group-prepend"">
                            <button class=""btn btn btn-primary submit"" type=""button"" onclick=""buscarPasajero()""><i class=""fa fa-search""></i></button>
                        </div>
                        <input maxlength=""8"" type=""text"" class=""form-control cedula col-sm-4 validate numeric"" placeholder=""Cedula de Pasajero"" id=""iCedula"">
                        <input type=""text"" class=""form-control col-sm-4 pItinerario validate"" id=""iNombre"" placeholder=""Nombres"" readonly>
                        <input type=""text"" class=""form-control col-sm-4 pItinerario validate"" id=""iApellido"" placeholder=""Apelidos"" readonly>
                    </div>
                </div>
                <div class=""form-group"">
                    <div class=""input-group mb-3"">
                        <div class=""input-group-prepend"">
                            <button cla");
                WriteLiteral(@"ss=""btn btn btn-primary submit"" type=""button"" onclick=""buscarVuelo()""><i class=""fa fa-search""></i></button>
                        </div>
                        <input maxlength=""4"" type=""text"" class=""form-control col-sm-3 validate numeric"" id=""vCodigo"" placeholder=""Codigo de vuelo"">
                        <input type=""text"" class=""form-control col-sm-3 vItineratio validate"" id=""vRuta"" placeholder=""Origen - Destino"" readonly>
                        <input type=""text"" class=""form-control col-sm-3 vItineratio validate"" id=""vPrecio"" placeholder=""Precio"" readonly>
                        <span class=""input-group-text"" id=""basic-addon1"">$</span>
                        <input maxlength=""3"" type=""text"" class=""form-control col-sm-3 vItineratio validate numeric"" name=""cantidad"" id=""vDisponibilidad"" placeholder=""Asientos"">
                        <span class=""input-group-text"" id=""basic-addon1"">Asientos</span>
                    </div>
                </div>
                <input type=""hidden"" id=""iId""");
                WriteLiteral(@" name=""idPasajeros"">
                <input type=""hidden"" id=""vId"" name=""idVuelos"">
                <input type=""hidden"" id=""vDisponibilidadHidden"">
                <button type=""submit"" class=""btn btn-primary float-right submit"" onclick=""validationFiels()"">Guardar</button>
");
                WriteLiteral("            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n</div>\r\n");
#nullable restore
#line 44 "C:\Users\José Meléndez\Desktop\test\travel-agency\travel-agency\Views\Itinerario\Edit.cshtml"
}

#line default
#line hidden
#nullable disable
            DefineSection("scripts", async() => {
                WriteLiteral(@"
<script type=""text/javascript"">
    
    //Validación solo números
    $('.numeric').on('input', function () {
        this.value = this.value.replace(/[^0-9]/g,'');
    });

    $('.letter').on('input', function () {
        this.value = this.value.replace(/[^a-záéíóúñA-ZÁÉÍÓÚÑ ]/g,'');
    });

    maxCantidad();

    function validationFiels() {
        Validar = $("".validate"");
        $(""form"").submit(function (e) {
            var x = false;
            Validar.each(function () {
                if ($(this).val() == """" || $(this).val() == null) {
                    x = true;
                    $(this).css(""border"", ""1px solid red"");
                } else {
                    $(this).css(""border"", ""1px solid lightgreen"");
                }
            });
            if (x) {
                e.preventDefault();
            } else {
                UpdateCantidad();
            }
        });
    }
</script>
");
            }
            );
            WriteLiteral("\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<travel_agency.Models.ViewModelItinerario> Html { get; private set; }
    }
}
#pragma warning restore 1591
