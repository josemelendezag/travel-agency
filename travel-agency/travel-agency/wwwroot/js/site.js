﻿
function EnableSubmit() {
    $(".submit").prop('disabled', false);
    $(".spinner-border").attr("hidden", true);
}
function DisableSubmit() {
    $(".submit").prop('disabled', true);
    $(".spinner-border").attr("hidden", false);
}

function Disablefiltel() {
    $("#search").prop("disabled", true);
    $("#FiltroApi").prop("disabled", true);
    $("#iconButton").removeClass("fa fa-search").addClass("spinner-border spinner-border-sm");
}

function EnableUpdate() {
    $(".update").prop('disabled', true);
}

function DisableUpdate() {
    $(".update").prop('disabled', false);
}

function AlertSuccess(alertmessage) {
    $("#AlertSuccessMessage").text(' ' + alertmessage);
    //$(".alert-success").fadeIn(750).delay(2000).fadeOut(300);
    $(".alert-success").fadeIn(200);
}

function AlertFailure(alertmessage) {
    $("#AlertFailureMessage").text(alertmessage);
    //$(".alert-danger").fadeIn(200).delay(2000).fadeOut(200);
    $(".alert-danger").fadeIn(200);
}

function AlertFailureClose() {
    $(".alert-danger").delay(100).fadeOut(100);
    //$(".alert-danger").fadeIn(200);
}

function Select2WithOutSearch() {
    $('.select2').val(tipo).select2({
        theme: 'bootstrap4',
        width: 'auto',
        minimumResultsForSearch: -1,
    });
}

function maxCantidad() {
    $('#vDisponibilidad').on('keyup', function () {
        let max = $('#vDisponibilidadHidden').val();
        let valor = parseInt(this.value);
        if (valor > max || isNaN(valor)) {
            DisableSubmit();
            $(this).css("border", "1px solid red");
            AlertFailure("La cantidad seleccionada no está disponible.");
            //this.value = max;
        } else {
            AlertFailureClose();
            $(this).css("border", "1px solid lightgreen");
            EnableSubmit();
        }
    });
};

function UpdateCantidad() {
    var disponibilidad = $("#vDisponibilidad").val();
    var disponibilidadH = $("#vDisponibilidadHidden").val();
    var res = disponibilidadH - disponibilidad;
    var id = $("#vId").val();
    partialUpdate(id, res);
}

function buscarPasajero() {
    var usuario = $("#iCedula").val();

    DisableSubmit();

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:9001/pasajeros/cedula",
        cache: false,
        global: false,
        data:
        {
            cedula: usuario
        },
        success: function (data) {
            EnableSubmit();
            if (data != undefined) {
                $("#iNombre").val(data["nombre"]);
                $("#iApellido").val(data["apellido"]);
                $("#iId").val(data["id"]);
            } else {
                $("pItinerario").val("");
                AlertFailure("Usuario no encontrado. Por favor, registrelo");
            }
        },
        error: function () {
            EnableSubmit();
            AlertFailure(" Ocurrió un error al intentar obtener datos");
        }
    });
}

function buscarVuelo() {
    var vuelo = $("#vCodigo").val();

    DisableSubmit();

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:9001/vuelos/codigo",
        cache: false,
        global: false,
        data:
        {
            codigo: vuelo
        },
        success: function (data) {
            EnableSubmit();
            if (data != undefined) {
                $("#vRuta").val(data["origen"] + " - " + data["destino"]);
                $("#vPrecio").val(data["precio"]);
                $("#vDisponibilidad").val(data["disponibilidad"]);
                $("#vDisponibilidadHidden").val(data["disponibilidad"]);
                $("#vId").val(data["id"]);
            } else {
                EnableSubmit();
                $(".vItineratio").val("");
                AlertFailure("Codigo de vuelo no encontrado.");
            }
        },
        error: function () {
            EnableSubmit();
            AlertFailure(" Ocurrió un error al intentar obtener datos");
        }
    });
}

function partialUpdate(id, cantidad) {
    $.ajax({
        url: 'http://localhost:9001/vuelos/partialedit/?id=' + id + '&cantidad=' + cantidad,
        //data: { disponibilidad: cantidad},
        type: 'PATCH',
        success: function () {
            return
        },
        error: function () {
            AlertFailure(" Ocurrió un error al intentar actualizar datos");
        }
    });    
}

//function validationFiels() {
//    Validar = $(".validate");
//    $("form").submit(function (e) {
//        var x = false;
//        Validar.each(function () {
//            if ($(this).val() == "" || $(this).val() == null) {
//                x = true;
//                $(this).css("border", "1px solid red");
//            } else {
//                $(this).css("border", "1px solid lightgreen");
//            }
//        });
//        if (x) {
//            e.preventDefault();
//        }
//        var disponibilidad = $("#vDisponibilidad").val();
//        var disponibilidadH = $("#vDisponibilidadHidden").val();
//        var res = disponibilidadH - disponibilidad;
//        var id = $("#vId").val();
//        partialUpdate(id, res);
//    });
//}

 //   function testSelect2() {
//        $(".select2").select2(
//        {
//            ajax: {
//                url: 'http://localhost:9001/pasajeros/cedula',
//                //url: 'http://localhost:9001/pasajeros/prueba',
//                dataType: 'json',
//                type: "GET",
//                delay: 250,
//                data: function (params) {
//                    return {
//                        cedula: params.term
//                    };
//                },
//                //processResults: function (data) {
//                //    var res = data.items.map(function (item) {
//                //        console.log("res " + res);
//                //        	return {id: item.id, text: item.cedula};
//                //    });
//                //    console.log("res " + res);
//                //    return {

//                //        results: res
//                //    };
//                //}
//                //processResults: function (data) {
//                //    return {
//                //        results: $.map(data, function (item) {
//                //            return { text: item.cedula, id: item.id}
//                //        })
//                //    };
//                //}
//                //processResults: function (data) {
//                //    return {
//                //        results: data
//                //    };
//                //}
//                processResults: function (data) {;
//                    //var result = new Array(data["id"], data["cedula"]);
//                    //var foo = [data["id"], data["cedula"]];

//                    //var test = JSON.stringify(Object.create(data["id"]: data["cedula"]));
//                    //var test3 = JSON.stringify(data["id"] : data["cedula"]);
 
//                    var test6 = '{"' + data["id"] + '":"' + data["cedula"] +'"}';
//                    var test5 = '{' + data["id"] + ':' + data["cedula"] +'}';
//                    return {
//                        results: $.map(test6, function (value, key) {
//                            var test7 = "x";
//                            conlose.log("Prueba");
//                            return {
//                                id: value,
//                                text: key
//                            }
//                        })
//                    };
//                }
//            },

//        });
//}

