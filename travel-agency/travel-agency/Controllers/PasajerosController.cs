﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using travel_agency.Models;

namespace travel_agency.Controllers
{
    [Authorize]
    public class PasajerosController : Controller
    {
        private readonly IConfiguration _configuration;
        public PasajerosController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Pasajeros()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("pasajeros/get");
                if (res.IsSuccessStatusCode)
                {
                    var listaPajajeros = res.Content.ReadAsStringAsync().Result;
                    var auditoresLista = JsonConvert.DeserializeObject<List<ViewModelPasajeros>>(listaPajajeros);
                    ViewData["listapasajeros"] = auditoresLista;
                }
            }
            return View();
        }

        //Muestra información formulario
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("pasajeros/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var pasajeroById = res.Content.ReadAsStringAsync().Result;
                    var pasajeroData = JsonConvert.DeserializeObject<ViewModelPasajeros>(pasajeroById);
                    ViewBag.Status= id == 0 ? 0 : 1;
                    //ViewData["pasajerodata"] = pasajeroData;
                    return View(pasajeroData);

                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Modified(ViewModelPasajeros data)
        {
            data.fmodificacion = DateTime.Now;
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var pasajeroModified = JsonConvert.SerializeObject(data);
                var content = new StringContent(pasajeroModified, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await client.PostAsync("pasajeros/edit", content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Pasajeros", "Pasajeros");
                }
            }
            return RedirectToAction("Pasajeros", "Pasajeros");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("pasajeros/delete/" + id);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Pasajeros", "Pasajeros");
                }
            }
            return RedirectToAction("Pasajeros", "Pasajeros");
        }

        [HttpPost]
        public async Task<IActionResult> Insert(ViewModelPasajeros data)
        {
            data.fcreacion = DateTime.Now;
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var pasajeroModified = JsonConvert.SerializeObject(data);
                var content = new StringContent(pasajeroModified, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await client.PostAsync("pasajeros/insert", content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Pasajeros", "Pasajeros");
                }
            }
            return RedirectToAction("Pasajeros", "Pasajeros");
        }
    }
}