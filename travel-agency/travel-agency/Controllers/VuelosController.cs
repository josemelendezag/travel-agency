﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using travel_agency.Models;

namespace travel_agency.Controllers
{
    [Authorize]
    public class VuelosController : Controller
    {
        private readonly IConfiguration _configuration;
        public VuelosController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Vuelos()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("vuelos/get");
                if (res.IsSuccessStatusCode)
                {
                    var getVuelos = res.Content.ReadAsStringAsync().Result;
                    var listaVuelos = JsonConvert.DeserializeObject<List<ViewModelViajes>>(getVuelos);
                    ViewData["listavuelos"] = listaVuelos;
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Insert(ViewModelViajes data)
        {
            data.fcreacion = DateTime.Now;
            data.precio = Math.Round(data.precio, 2);
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var vModified = JsonConvert.SerializeObject(data);
                var content = new StringContent(vModified, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await client.PostAsync("vuelos/insert", content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Vuelos", "Vuelos");
                }
            }
            return RedirectToAction("Vuelos", "Vuelos");
        }

        //Muestra información formulario
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("vuelos/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var vId = res.Content.ReadAsStringAsync().Result;
                    var vData = JsonConvert.DeserializeObject<ViewModelViajes>(vId);
                    ViewBag.Status = id == 0 ? 0 : 1;
                    return View(vData);
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Modified(ViewModelViajes data)
        {
            data.fmodificacion = DateTime.Now;
            data.precio = Math.Round(data.precio, 2);
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var vModified = JsonConvert.SerializeObject(data);
                var content = new StringContent(vModified, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await client.PostAsync("vuelos/edit", content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Vuelos", "Vuelos");
                }
            }
            return RedirectToAction("Vuelos", "Vuelos");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("vuelos/delete/" + id);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Vuelos", "Vuelos");
                }
            }
            return RedirectToAction("Vuelos", "Vuelos");
        }
    }
}