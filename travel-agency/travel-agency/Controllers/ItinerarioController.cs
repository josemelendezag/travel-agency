﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using travel_agency.Models;

namespace travel_agency.Controllers
{
    [Authorize]
    public class ItinerarioController : Controller
    {
        private readonly IConfiguration _configuration;
        public ItinerarioController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IActionResult> Itinerario()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("itinerario/get");
                if (res.IsSuccessStatusCode)
                {
                    var getVuelos = res.Content.ReadAsStringAsync().Result;
                    var listaVuelos = JsonConvert.DeserializeObject<List<ViewModelItinerario>>(getVuelos);
                    ViewData["listaitinerario"] = listaVuelos;
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Insert(ViewModelItinerario data)
        {
            data.fcreacion = DateTime.Now;
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var iModified = JsonConvert.SerializeObject(data);
                var content = new StringContent(iModified, Encoding.UTF8, "application/json");
                HttpResponseMessage res = await client.PostAsync("itinerario/insert", content);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Itinerario", "Itinerario");
                }
            }
            return RedirectToAction("Itinerario", "Itinerario");
        }

        //Muestra información formulario
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("itinerario/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var iId = res.Content.ReadAsStringAsync().Result;
                    var iData = JsonConvert.DeserializeObject<ViewModelItinerario>(iId);
                    ViewBag.Status = id == 0 ? 0 : 1;


                    return View(iData);
                }
            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> getCedulaPasajero(int cedula)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("pasajero/" + cedula);
                if (res.IsSuccessStatusCode)
                {
                    var pCedula = res.Content.ReadAsStringAsync().Result;
                    var pData = JsonConvert.DeserializeObject<ViewModelPasajeros>(pCedula);
                    return new JsonResult(pData);
                }
            }
            return NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("itinerario/delete/" + id);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Itinerario", "Itinerario");
                }
            }
            return RedirectToAction("Itinerario", "Itinerario");
        }

    }
}