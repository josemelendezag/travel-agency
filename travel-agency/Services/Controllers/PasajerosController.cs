﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Services.Code;

namespace Services.Controllers
{
    [ApiController]
    [Route("/pasajeros")]
    public class PasajerosController : ControllerBase
    {
        private readonly DbContextOptions<DataAccessContext> _dbOptions;

        public PasajerosController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<DataAccessContext>>();
        }

        [HttpGet("get")]
        public List<Pasajeros> GetPasajeros()
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                return _context.Pasajeros.ToListAsync().Result;
            }
        }

        [HttpGet("{id}")]
        public Pasajeros GetByIdPasajeros(int id)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var pasajero = _context.Pasajeros.Find(id);
                if (pasajero == null) NotFound();
                return pasajero;
            }
        }

        [HttpGet("cedula")]
        public Pasajeros GetByCedulaPasajeros(int cedula)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var pCedula = _context.Pasajeros.FirstOrDefaultAsync(c => c.cedula == cedula).Result;
                if (pCedula == null) NotFound();
                return pCedula;
            }
        }

        [HttpPost("insert")]
        public ActionResult InsertPasajeros(Pasajeros data)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var pasajero = _context.Pasajeros.FirstOrDefaultAsync(c=>c.cedula == data.cedula).Result;
                if (pasajero != null) return NotFound();
                _context.Pasajeros.Add(data);
                _context.SaveChanges();
                return NoContent();
            }
        }

        //[HttpPut]
        [HttpPost("edit")]
        public ActionResult UpdatePasajeros([FromBody] Pasajeros data)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                _context.Entry(data).State = EntityState.Modified;
                _context.SaveChanges();
                return NoContent();
            }
        }

        //[HttpDelete("{id}")]
        [HttpGet("delete/{id}")]
        public ActionResult DeletePasajeros(int id)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var deletePasajero = _context.Pasajeros.Find(id);
                if (deletePasajero == null) return NotFound();

                _context.Pasajeros.Remove(deletePasajero);
                _context.SaveChanges();

                return NoContent();
            }
        }

    }
}