﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Services.Code;

namespace Services.Controllers
{
    [ApiController]
    [Route("/itinerario")]
    public class ItinerarioController : ControllerBase
    {
        private readonly DbContextOptions<DataAccessContext> _dbOptions;

        public ItinerarioController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<DataAccessContext>>();
        }

        [HttpGet("get")]
        public List<Itinerario> GetItinerario()
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                //var result =_context.ItinerarioAll.ex ExecuteSqlRaw(query).;
                var result =_context.Itinerario.ToListAsync().Result;
                
                return result;
            }
        }

        [HttpGet("{id}")]
        public Itinerario GetByIdItinerario(int id)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var itinerario = _context.Itinerario.Find(id);
                if (itinerario == null) NotFound();
                return itinerario;
            }
        }

        [HttpPost("insert")]
        public ActionResult InsertItinerario([FromBody]Itinerario data)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                //var Itinerario = _context.Itinerario.FirstOrDefaultAsync(c => c.codigo == data.codigo).Result;
                //if (vuelo != null) return NotFound();
                _context.Itinerario.Add(data);
                _context.SaveChanges();
                return NoContent();
            }
        }

        [HttpGet("delete/{id}")]
        public ActionResult DeleteItinerario(int id)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var deleteItinerario = _context.Itinerario.Find(id);
                if (deleteItinerario == null) return NotFound();

                _context.Itinerario.Remove(deleteItinerario);
                _context.SaveChanges();

                return NoContent();
            }
        }






    }
}