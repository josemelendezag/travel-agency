﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Services.Code;


namespace Services.Controllers
{
    [ApiController]
    [Route("/vuelos")]
    public class VuelosController : ControllerBase
    {
        private readonly DbContextOptions<DataAccessContext> _dbOptions;

        public VuelosController(IServiceProvider serviceProvider)
        {
            _dbOptions = serviceProvider.GetService<DbContextOptions<DataAccessContext>>();
        }

        [HttpGet("get")]
        public List<Vuelos> GetVuelos()
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                return _context.Vuelos.ToListAsync().Result;
            }
        }

        [HttpGet("{id}")]
        public Vuelos GetByIdVuelos(int id)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var vuelo = _context.Vuelos.Find(id);
                if (vuelo == null) NotFound();
                return vuelo;
            }
        }

        [HttpGet("codigo")]
        public Vuelos GetByCodigoVuelos(int codigo)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var vuelo = _context.Vuelos.FirstOrDefaultAsync(c => c.codigo == codigo).Result;
                if (vuelo == null) NotFound();
                return vuelo;
            }
        }
        
        [HttpPost("insert")]
        public ActionResult InsertVuelos([FromBody]Vuelos data)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var vuelo = _context.Vuelos.FirstOrDefaultAsync(c => c.codigo == data.codigo).Result;
                if (vuelo != null) return NotFound();
                _context.Vuelos.Add(data);
                _context.SaveChanges();
                return NoContent();
            }
        }

        [HttpPost("edit")]
        public ActionResult UpdateVuelos([FromBody] Vuelos data)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                _context.Entry(data).State = EntityState.Modified;
                _context.SaveChanges();
                return NoContent();
            }
        }

        [HttpPatch("partialedit")]
        public ActionResult PartialUpdateVuelos([FromQuery] int id, [FromQuery] int cantidad)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var vuelo = _context.Vuelos.Find(id);
                vuelo.disponibilidad = cantidad;
                _context.SaveChanges();
                return NoContent();
            }
        }

        [HttpGet("delete/{id}")]
        public ActionResult DeleteVuelos(int id)
        {
            using (DataAccessContext _context = new DataAccessContext(_dbOptions))
            {
                var dVuelo = _context.Vuelos.Find(id);
                if (dVuelo == null) return NotFound();

                _context.Vuelos.Remove(dVuelo);
                _context.SaveChanges();

                return NoContent();
            }
        }

    }
}