﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Code
{
    public class Vuelos
    {
        [Key]
        public int id { get; set; }
        public int codigo { get; set; }
        public string origen { get; set; }
        public string destino { get; set; }
        public int disponibilidad { get; set; }
        public double precio { get; set; }
        public DateTime? fcreacion { get; set; }
        public DateTime? fmodificacion { get; set; }
    }
}
