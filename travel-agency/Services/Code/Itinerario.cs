﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Services.Code
{
    public class Itinerario
    {
        [Key]
        public int id { get; set; }
        public int idPasajeros { get; set; }
        public int idVuelos { get; set; }
        public int cantidad { get; set; }
        public DateTime? fcreacion { get; set; }
        public DateTime? fmodificacion { get; set; }
    }
}
