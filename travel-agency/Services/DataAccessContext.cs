﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Services.Code;

namespace Services
{
    public class DataAccessContext : DbContext
    {
        public DataAccessContext(DbContextOptions<DataAccessContext> options) : base(options)
        { }

        public DbSet<Pasajeros> Pasajeros { get; set; }
        public DbSet<Vuelos> Vuelos { get; set; }
        public DbSet<Itinerario> Itinerario { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Pasajeros>(eb =>
        //        {
        //            eb.HasKey(c => c.cedula);
        //        });

        //}
    }
}
